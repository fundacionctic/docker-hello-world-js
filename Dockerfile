FROM node:8-stretch

COPY helloworld.js /root

RUN npm install http

EXPOSE 55555

CMD node /root/helloworld.js

